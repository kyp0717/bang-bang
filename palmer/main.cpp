#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <signal.h>

#include "server.h"

Server serv;
using namespace std;

int main(int argc, char const* argv[])
{
    set_handler();
    vector<string> results = serv.poll_messages();
}

// Handle CTRL-C gracefully
void event_handler(int s){
    if(s == 2) {
        cout << "\nCaught Signal " << s << " and Exiting...\n";
        // serv.kill(); Not necessary as destructor working
        serv.looping = 0;
        exit(0);
    }
}

void set_handler(void) {
    // create sigaction handler to catch CTRL-C signals
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = event_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);
}