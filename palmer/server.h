#include <vector>

class Server {
	private:
		// variables for socket interface
		int server_fd, new_socket, valread, int_port;
		struct sockaddr_in address;
		int opt = 1;
		int addrlen = sizeof(address);
		char buffer[1024] = { 0 };

		int locked = 1; // commands locked until security passed
		std::string secret; // password string

		void init(void); // start socket server (called by constructor)
		void load_secret(void); // load password from file
		void get_net_options(void); // get port from enviroment variable

	public:
		Server(); // constructor
		~Server(); // destructor
		void fire(void); // reads "data" and transfers it to client
		void kill(void); // close socket and destroy class
		char *get_message(void); // await and return a message
		std::vector<std::string> poll_messages(void); // continuously get messages
		int looping = 1;
};

// must only declare the functions once
#ifndef SERVER_HEADER
#define SERVER_HEADER

void event_handler(int s);
void set_handler(void);

#endif // MY_HEADER_H_INC
