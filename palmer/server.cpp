// Server side C++ program based on UNIX sockets
// Listens for messages and returns data
/* TODO ::


*/
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>

#include "server.h"

#define PORT 8080
using namespace std;
	
Server::Server(){
	// Constructor calls init
	load_secret();
	get_net_options();
	init();
}

Server::~Server(){
	// close and shutdown socket
	kill();
}

void Server::init(void) {
	// Create socket file descriptor
	std::cout << "Initializing server...\n";
	
	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd < 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	// Attach socket to port 8080
	if (setsockopt(server_fd, SOL_SOCKET,
				SO_REUSEADDR | SO_REUSEPORT, &opt,
				sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(int_port);

	// Attach socket to the port 8080
	if (bind(server_fd, (struct sockaddr*)&address,
			sizeof(address))
		< 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
}

void Server::kill(void){
	// close and shutdown socket
	std::cout << "Server destroyed\n";
	close(new_socket);
	shutdown(server_fd, SHUT_RDWR);
}

void Server::get_net_options(void) {
	// TODO - bind server to specific interface instead of accepting all

	// load port option from enviroment variable
	char *ip_port = getenv("LAZ_IP_PORT");
	int_port = PORT; // set default port
	if (ip_port) { // convert port option to int if found
		int_port = atoi(ip_port);
	}
	cout << "PORT: " << int_port << endl;
	cout << "Use LAZ_IP_PORT env variables to change option." << endl;
}

void Server::load_secret(void){
	// load password from secret file
	std::ifstream file("secret");
	if(file) {
		secret.assign( (std::istreambuf_iterator<char>(file) ),
    			(std::istreambuf_iterator<char>()    ) );
		cout << "secret set: " << secret << endl;
	}
	else { // unlock server if no secret file found
		locked = 0;
		cout << "No secret found. Server unlocked" << endl;
	}
}

char *Server::get_message(void){
	// Listen for message and accept it
	if (listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	if ((new_socket
		= accept(server_fd, (struct sockaddr*)&address,
				(socklen_t*)&addrlen))
		< 0) {
		perror("accept");
		exit(EXIT_FAILURE);
	}

	// read the message into buffer
    memset( buffer, 0x00, 1024 ); // clear buffer
	int bytes_read = read(new_socket, buffer, 1024);

	// block messages if server locked
	if(locked) {
		string s(buffer);
		memset(buffer, 0x00, 1024);
		if(s == secret) { // unlock server if password matches
			locked = 0;
		}
	}
	return buffer;
}

vector<string> Server::poll_messages(void) {
	// Loop until looping set 0 or destructor called
	// calling get_message each loop
	vector<string> messages;
	cout << "Listening for message: " << std::flush;

	while(looping){
		string result = get_message();


		if (result == "EXIT"){
			cout << "Received EXIT message\n";
			looping = 0;
			break;
		}
		else if (result == "FIRE") {
			cout << "Received FIRE message\n";
			fire();
		}
		else if(result != "") {
			cout << result << endl;
			messages.push_back(result);
		}
		cout << "listening for message: " << std::flush;
	}
	return messages;
}

void Server::fire(void) {
    int n, total_bytes = 0; 
    char buffer[BUFFER_SIZE] = {0}; 

	// open file as test data
    FILE *fp = fopen("data", "rb");
    if (fp == NULL) 
    {
        perror("Can't open file");
        exit(1);
    } 

	// read and transfer file in BUFFER_SIZE chunks
	// loop until end of file (n = zero bytes)
    while ((n = fread(buffer, sizeof(char), BUFFER_SIZE, fp)) > 0) 
    {
	    total_bytes+=n;
        if (n != BUFFER_SIZE && ferror(fp))
        {
            perror("File read error");
            exit(1);
        }

		// send next file chunk
        if (send(new_socket, buffer, n, 0) == -1)
        {
            perror("File send error");
			fclose(fp);
            exit(1);
        }
        memset(buffer, 0, BUFFER_SIZE); // clear buffer
    }
	close(new_socket);
	fclose(fp);
	printf("Data sent: %d bytes\n", total_bytes);
}

